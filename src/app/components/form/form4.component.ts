import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-form4',
  templateUrl: './form4.component.html',
  styleUrls: ['./form4.component.css']
})
export class FormComponent implements OnInit {

  form!: FormGroup;
  array: string[] = []


  constructor(private fb: FormBuilder) { 
    this.crearFormulario();

  }

  crearFormulario(): void{
    this.form = this.fb.group({
      boxInput: this.fb.array([[]]),
      
    })


  }

  ngOnInit(): void {
  }

  get newInput(){
    return this.form.get('boxInput') as FormArray
  }

  agregar():void{
    this.newInput.push(this.fb.control('', Validators.required))
    console.log(this.newInput);
    
  }
  borrarCaja(i: number): void{
    this.newInput.removeAt(i);
  }

  limpiar(): void {
    this.array = ['']
    this.form.reset();
  }
  limpiarText(): void{
    this.array = ['']
  }

  limpiarText1(): void {
    this.form.reset(this.newInput.value.i);
  }

  guardar(): void{
    console.log('guardar');
    this.array = this.form.value.boxInput
  }

}
